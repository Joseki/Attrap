make: ppparis pref04 pref05 pref06 pref09 pref13 pref31 pref33 pref34 pref35 pref38 pref42 pref44 pref47 pref59 pref62 pref63 pref64 pref65 pref66 pref69 pref80 pref81 pref83 pref87 pref93 pref976
ppparis:
	bin/python3 cli.py ppparis
pref04:
	bin/python3 cli.py pref04
pref05:
	bin/python3 cli.py pref05
pref06:
	bin/python3 cli.py pref06
pref09:
	bin/python3 cli.py pref09
pref12:
	bin/python3 cli.py pref12
pref13:
	bin/python3 cli.py pref13
pref31:
	bin/python3 cli.py pref31
pref33:
	bin/python3 cli.py pref33
pref34:
	bin/python3 cli.py pref34
pref35:
	bin/python3 cli.py pref35
pref38:
	bin/python3 cli.py pref38
pref40:
	bin/python3 cli.py pref40
pref42:
	bin/python3 cli.py pref42
pref44:
	bin/python3 cli.py pref44
pref46:
	bin/python3 cli.py pref46
pref47:
	bin/python3 cli.py pref47
pref59:
	bin/python3 cli.py pref59
pref62:
	bin/python3 cli.py pref62
pref63:
	bin/python3 cli.py pref63
pref64:
	bin/python3 cli.py pref64
pref65:
	bin/python3 cli.py pref65
pref66:
	bin/python3 cli.py pref66
pref69:
	bin/python3 cli.py pref69
pref80:
	bin/python3 cli.py pref80
pref81:
	bin/python3 cli.py pref81
pref83:
	bin/python3 cli.py pref83
pref87:
	bin/python3 cli.py pref87
pref93:
	bin/python3 cli.py pref93
pref976:
	bin/python3 cli.py pref976
lint:
	bin/pycodestyle --first --show-source --ignore=E501 *.py
