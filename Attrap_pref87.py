import os
import datetime

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap


class Attrap_pref87(Attrap):

    # Config
    __HOST = 'https://www.haute-vienne.gouv.fr'
    __RAA_PAGE = {
        '2024': [
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/JANVIER-JUIN-2024/JANVIER-JUIN-2024',
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/JUILLET-DECEMBRE-2024'
        ],
        '2023': [
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/JANVIER-JUIN-2023',
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/JUILLET-DECEMBRE-2023/JUILLET-DECEMBRE-2023'
        ],
        '2022': [
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/JANVIER-JUIN-2022',
            f'{__HOST}/Publications/Recueil-des-actes-administratifs/JUILLET-DECEMBRE-2022/Recueil-des-actes-administratifs-2022',
        ],
        '2021': [f'{__HOST}/Publications/Recueil-des-actes-administratifs/Archives-des-recueils-des-actes-administratifs/2021'],
        '2020': [f'{__HOST}/Publications/Recueil-des-actes-administratifs/Archives-des-recueils-des-actes-administratifs/2020'],
        '2019': [f'{__HOST}/Publications/Recueil-des-actes-administratifs/Archives-des-recueils-des-actes-administratifs/2019']
    }
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'
    full_name = 'Préfecture de la Haute-Vienne'
    short_code = 'pref87'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.enable_tor(10)

    def get_raa(self, keywords):
        year_pages_to_parse = []
        if self.not_before.year <= 2024:
            for year_page in self.__RAA_PAGE['2024']:
                year_pages_to_parse.append(year_page)
        if self.not_before.year <= 2023:
            for year_page in self.__RAA_PAGE['2023']:
                year_pages_to_parse.append(year_page)
        if self.not_before.year <= 2022:
            for year_page in self.__RAA_PAGE['2022']:
                year_pages_to_parse.append(year_page)
        if self.not_before.year <= 2021:
            for year_page in self.__RAA_PAGE['2021']:
                year_pages_to_parse.append(year_page)
        if self.not_before.year <= 2020:
            for year_page in self.__RAA_PAGE['2020']:
                year_pages_to_parse.append(year_page)
        if self.not_before.year <= 2019:
            for year_page in self.__RAA_PAGE['2019']:
                year_pages_to_parse.append(year_page)

        pages_to_parse = year_pages_to_parse
        # Pour chaque année, on cherche les éventuelles sous-pages de mois
        for year_page in year_pages_to_parse:
            page_content = self.get_page(year_page, 'get').content
            month_pages = self.get_sub_pages(
                page_content,
                '.fr-card.fr-card--sm.fr-card--grey.fr-enlarge-link div.fr-card__body div.fr-card__content h2.fr-card__title a',
                self.__HOST,
                False
            )[::-1]

            # On filtre les pages de mois ne correspondant pas à la période analysée
            for month_page in month_pages:
                guessed_date = Attrap.guess_date(month_page['name'], '([a-zéû]* [0-9]{4})').replace(day=1)
                if guessed_date >= self.not_before.replace(day=1):
                    pages_to_parse.append(month_page['url'])

        # On parse les pages contenant des RAA
        elements = []
        for page in pages_to_parse:
            page_content = self.get_page(page, 'get').content
            for raa in self.get_raa_elements(page_content):
                elements.append(raa)

        self.parse_raa(elements, keywords)
        self.mailer()

    def get_raa_elements(self, page_content):
        elements = []
        # On charge le parser
        soup = BeautifulSoup(page_content, 'html.parser')

        # On récupère chaque balise a
        for a in soup.select('a.fr-link.fr-link--download'):
            if a.get('href') and a['href'].endswith('.pdf'):
                if a['href'].startswith('/'):
                    url = f"{self.__HOST}{a['href']}"
                else:
                    url = a['href']

                url = unquote(url)
                name = a.find('span').previous_sibling.replace('Télécharger ', '').strip()
                date = datetime.datetime.strptime(a.find('span').get_text().split(' - ')[-1].strip(), '%d/%m/%Y')

                raa = Attrap.RAA(url, date, name)
                elements.append(raa)
        return elements
