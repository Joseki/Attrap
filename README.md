# Attrap

Un logiciel qui récupère les derniers recueils des actes administratifs (RAA) pour y rechercher certains mots-clés prédéfinis.

Conçu pour être utilisé dans une CI.

Peut envoyer par email et sur Mastodon  les résultats, par exemple avec <a rel="me" href="https://mamot.fr/@AttrapSurveillance">@AttrapSurveillance\@mamot.fr</a>.

## Installation

Il est recommandé d'utiliser virtualenv :

```bash
virtualenv --python=/usr/bin/python3 .
source bin/activate
pip3 install -r requirements.txt
```

Vous devez avoir installé OCRmyPDF, les données `eng` et `fra` de Tesseract, et le démon Tor.

## Utilisation

Pour lancer la récupération de toutes les administrations supportées :

```bash
make
```

Attention, le premier lancement prendra plusieurs jours ! Si vous utilisez une CI, vous devez mettre en cache le dossier `data/` afin que les fichiers déjà analysés ne soient pas téléchargés à chaque lancement.

Il est possible de ne lancer l'analyse que pour une seule administration, avec la commande : `./cli.py identifiant`

## Options

Les options suivantes peuvent être précisées, par un paramètre si l'utilitaire `cli.py` est utilisé, ou par une variable d'environnement :

| CLI | Variable d'environnement | Signification | Valeur par défaut |
|---|---|---|---|
| `--keywords`, `-k` | `KEYWORDS` | Liste des mots-clés recherchés, séparés par une virgule. | Aucune |
| `--not-before` | `NOT_BEFORE` | Date (format YYYY-MM-DD) avant laquelle les RAA ne sont pas analysés. | `2024-01-01` |
| `--smtp-hostname` | `SMTP_HOSTNAME` | Nom d'hôte SMTP. | `localhost` |
| `--smtp-username` | `SMTP_USERNAME` | Nom d'utilisateur SMTP. | Aucun |
| `--smtp-password` | `SMTP_PASSWORD` | Mot de passe SMTP. | Aucun |
| `--smtp-port` | `SMTP_PORT` | Port SMTP. | `587` |
| `--smtp-starttls` | `SMTP_STARTTLS` | Si spécifié, la connexion SMTP se fait avec STARTTLS. | Non-spécifié |
| `--smtp-ssl` | `SMTP_SSL` | Si spécifié, la connexion SMTP se fait avec SSL. | Non-spécifié |
| `--email-from`, `-f` | `EMAIL_FROM` | Adresse de courrier électronique expéditrice des notifications. | Aucune (désactive l'envoi) |
| `--email-to`, `-t` | `EMAIL_TO` | Adresses de courriers électroniques destinataires des notifications, séparées par une virgule. | Aucune (désactive l'envoi) |
| `--**-email-to` | `--**-EMAIL-TO` | Pour chaque administration dont l'identifiant est **, adresses de courriers électroniques destinataires des notifications, séparées par une virgule, uniquement si l'analyse concerne cette administration en particulier. La liste s'ajoute à celle précisée dans `--email-to`. | Aucune |
| `--mastodon-access-token` | `MASTODON_ACCESS_TOKEN` | Jeton d'accès pour publier sur Mastodon. | Aucun (désactive la publication sur Mastodon) |
| `--mastodon-instance` | `MASTODON_INSTANCE` | URL de l'instance Mastodon de publication (doit inclure "http://" ou "https://"). | Aucune (désactive la publication sur Mastodon) |
| `-v` | `VERBOSE` | Si spécifié, relève le niveau de verbosité à INFO. | Non-spécifié |
| `-vv` | `VVERBOSE` | Si spécifié, relève le niveau de verbosité à DEBUG. | Non-spécifié |

## Administrations supportées

- Préfecture de police de Paris (identifiant : `ppparis`)
- Préfecture des Alpes-de-Haute-Provence (identifiant : `pref04`)
- Préfecture des Hautes-Alpes (identifiant : `pref05`)
- Préfecture des Alpes-Maritimes (identifiant : `pref06`)
- Préfecture de l'Ariège (identifiant : `pref09`)
- Préfecture des Bouches-du-Rhône (identifiant : `pref13`)
- Préfecture de la Haute-Garonne (identifiant : `pref31`)
- Préfecture de la Gironde (identifiant : `pref33`)
- Préfecture de l'Hérault (identifiant : `pref34`)
- Préfecture d'Ille-et-Vilaine (identifiant : `pref35`)
- Préfecture de l'Isère (identifiant : `pref38`)
- Préfecture de la Loire (identifiant : `pref42`)
- Préfecture de la Loire-Atlantique (identifiant : `pref44`)
- Préfecture du Nord (identifiant : `pref59`)
- Préfecture du Pas-de-Calais (identifiant : `pref62`)
- Préfecture des Pyrénées-Atlantiques (identifiant : `pref64`)
- Préfecture des Hautes-Pyrénées (identifiant : `pref65`)
- Préfecture des Pyrénées-Orientales (identifiant : `pref66`)
- Préfecture du Rhône (identifiant : `pref69`)
- Préfecture de la Somme (identifiant : `pref80`)
- Préfecture du Tarn (identifiant : `pref81`)
- Préfecture du Var (identifiant : `pref83`)
- Préfecture de la Haute-Vienne (identifiant : `pref87`)
- Préfecture de Seine-Saint-Denis(identifiant : `pref93`)
- Préfecture de Mayotte (identifiant : `pref976`)

## Contributions

Les contributions à ce projet sont les bienvenues ! 

Vous pouvez rejoindre le salon de discussion à ce sujet sur Matrix : `#Attrap:laquadrature.net`

## Licence

[CeCILL_V2.1-fr](https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html) (voir le fichier `LICENSE`)
