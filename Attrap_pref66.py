import os
import sys
import datetime
import logging

from bs4 import BeautifulSoup
from urllib.parse import unquote

from Attrap import Attrap

logger = logging.getLogger(__name__)


class Attrap_pref66(Attrap):

    # Config
    __HOST = 'https://www.pyrenees-orientales.gouv.fr'
    __RAA_PAGE = {
        '2024': f'{__HOST}/Publications/Le-recueil-des-actes-administratifs/Annee-2024',
        '2023': f'{__HOST}/Publications/Le-recueil-des-actes-administratifs/Annee-2023',
        '2022': f'{__HOST}/Publications/Le-recueil-des-actes-administratifs/Annee-2022',
        '2021': f'{__HOST}/Publications/Le-recueil-des-actes-administratifs/Annee-2021',
        '2020': f'{__HOST}/Publications/Le-recueil-des-actes-administratifs/Annee-2020',
        '2019': f'{__HOST}/Publications/Le-recueil-des-actes-administratifs/Annee-2019'
    }
    __USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0'
    full_name = 'Préfecture des Pyrénées-Orientales'
    short_code = 'pref66'

    def __init__(self, data_dir):
        super().__init__(data_dir, self.__USER_AGENT)
        self.enable_tor(10)

    def get_raa(self, keywords):
        elements = []

        # La préfecture des Pyrénées-Orientales est une originale : avant 2024,
        # chaque page annuelle contient l'ensemble des RAA, mais pas tout le
        # temps avec leur date, qu'il faut deviner à partir du nom du RAA.
        # Mais en 2024, ça change ! La page de 2024 contient un tableau
        # récapitulatif avec toutes les dates de publication des RAA, mais
        # aussi un pager. Sauf qu'il s'avère que le tableau récapitulatif
        # n'est pas exhaustif. On doit donc parser toutes les sous-pages de
        # 2024 puisqu'on ne peut se fier au tableau récapitulatif.
        # Grrr.
        if self.not_before.year <= 2024:
            for element in self.get_raa_elements_since_2024(self.__RAA_PAGE['2024']):
                elements.append(element)
        if self.not_before.year <= 2023:
            for element in self.get_raa_elements_before_2024(self.__RAA_PAGE['2023']):
                elements.append(element)
        if self.not_before.year <= 2022:
            for element in self.get_raa_elements_before_2024(self.__RAA_PAGE['2022']):
                elements.append(element)
        if self.not_before.year <= 2021:
            for element in self.get_raa_elements_before_2024(self.__RAA_PAGE['2021']):
                elements.append(element)
        if self.not_before.year <= 2020:
            for element in self.get_raa_elements_before_2024(self.__RAA_PAGE['2020']):
                elements.append(element)
        if self.not_before.year <= 2019:
            for element in self.get_raa_elements_before_2024(self.__RAA_PAGE['2019']):
                elements.append(element)

        self.parse_raa(elements, keywords)
        self.mailer()

    # On parse un lien d'avant 2024
    def get_raa_elements_before_2024(self, page):
        elements = []
        page_content = self.get_page(page, 'get').content
        soup = BeautifulSoup(page_content, 'html.parser')
        for a in soup.select('div.fr-table.fr-table--bordered.list a.fr-link.fr-link--download'):
            if a.get('href') and a['href'].endswith('.pdf'):
                date = None
                try:
                    # Lorsque la date n'est pas affichée à l'écran, elle est en
                    # fait cachée dans la propriété "title" du lien
                    details = ''
                    if a.find('span'):
                        details = a.find('span').get_text().split(' - ')[-1].strip()
                    else:
                        details = a['title'].split(' - ')[-1].strip()
                    date = datetime.datetime.strptime(details, '%d/%m/%Y')
                except Exception as exc:
                    logger.error(f'Impossible de trouver de date pour le texte : {text_raw}: {exc}')
                    sys.exit(1)

                if date >= self.not_before:
                    url = ''
                    if a['href'].startswith('/'):
                        url = f"{self.__HOST}{a['href']}"
                    else:
                        url = a['href']

                    url = unquote(url)
                    name = ''
                    if a.find('span') and a.find('span').previous_sibling:
                        name = a.find('span').previous_sibling.replace('Télécharger ', '').strip()
                    else:
                        name = a.get_text().replace('Télécharger ', '').strip()

                    elements.append(Attrap.RAA(url, date, name))
        return elements

    # On parse les RAA depuis 2024
    def get_raa_elements_since_2024(self, root_page):
        pages = self.get_sub_pages_with_pager(
            root_page,
            'div.fr-card__body div.fr-card__content h2.fr-card__title a.fr-card__link',
            'ul.fr-pagination__list li a.fr-pagination__link.fr-pagination__link--next',
            'div.fr-card__body div.fr-card__content div.fr-card__end p.fr-card__detail',
            self.__HOST
        )[::-1]

        pages_to_parse = []
        elements = []

        for page in pages:
            if not page['url'].endswith('.pdf'):
                logger.warning(f"Attention, le lien vers {page['url']} n'est pas bon !")
            else:
                if page['url'].startswith('/'):
                    url = f"{self.__HOST}{page['url']}"
                else:
                    url = page['url']

                url = unquote(url)
                name = page['name'].replace('Télécharger ', '').strip()
                date = datetime.datetime.strptime(page['details'].replace('Publié le ', '').strip(), '%d/%m/%Y')

                elements.append(Attrap.RAA(url, date, name))
        return elements
